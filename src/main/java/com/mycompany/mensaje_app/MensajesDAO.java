package com.mycompany.mensaje_app;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class MensajesDAO {

    public static void CrearMensajeDB(Mensajes mensaje) {

        Conexion db_connect = new Conexion();

        try (Connection conexion = db_connect.get_connection()) {

            PreparedStatement ps = null;

            try {
                String query = "INSERT INTO `mensajes` (`mensaje`, `autor_mensaje`) VALUES (? , ?);";
                ps = conexion.prepareStatement(query);
                ps.setString(1, mensaje.getMensaje());
                ps.setString(2, mensaje.getAutor_mensaje());
                ps.executeUpdate();
                System.out.println("mensaje creado correctamente");
            } catch (SQLException e) {

                System.out.println(e);

            }

        } catch (SQLException e) {

            System.out.println(e);

        }
    }

    public static void leerMensajesDB() {

        Conexion db_connect = new Conexion();

        PreparedStatement ps = null;
        ResultSet rs = null;

        try (Connection conexion = db_connect.get_connection()) {

            String query = "SELECT * FROM `mensajes`";
            ps = conexion.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {

                System.out.println("///////////////////////////////////"
                        + "\n ID: " + rs.getInt("id_mensaje")
                        + "\n Mensaje: " + rs.getString("mensaje")
                        + "\n Autor: " + rs.getString("autor_mensaje")
                        + "\n Fecha: " + rs.getString("fecha")
                        + "\n ///////////////////////////////////");
            }

        } catch (SQLException e) {
            System.out.println("no se recuperaron los mensajes"
                    + "\n" + e);

        }

    }

    public static void borrarMensajeDB(int id_mensaje) {

        Conexion db_connect = new Conexion();

        try (Connection conexion = db_connect.get_connection()) {
            PreparedStatement ps = null;

            try {

                String query = "DELETE FROM mensajes WHERE id_mensaje = ?";
                ps = conexion.prepareStatement(query);
                ps.setInt(1, id_mensaje);
                ps.executeUpdate();
                System.out.println("el mensaje se elimino con exito");

            } catch (SQLException e) {

                System.out.println("no se pudo borrar el mensaje ////  " + e);

            }

        } catch (SQLException e) {
            System.out.println("no se ha eliminado el mensaje o no existe"
                    + "\n" + e);

        }

    }

    public static void actualizarMensajeDB(Mensajes mensaje) {

        Conexion db_connect = new Conexion();
        
        try(Connection conexion = db_connect.get_connection()){
               PreparedStatement ps = null;
               
               
               try{
                   
                   String query = "UPDATE mensajes SET mensaje = ? WHERE id_mensaje = ?";
                   ps = conexion.prepareStatement(query);
                   ps.setString(1, mensaje.getMensaje());
                   ps.setInt(2, mensaje.id_mensaje);
                   ps.executeUpdate();
                   System.out.println("Mensaje se actualizo correctamente");
                   
               }catch(SQLException e){

                   System.out.println("no se pudo editar el texto //// " + 
                           "\n " + e );
                   
               }
            
        }catch(SQLException e){
            System.out.println(e);
        }

    }

}
