package com.mycompany.mensaje_app;

import java.util.Scanner;

public class mensajesService {

    public static void crearMensaje() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Escribe tu mensaje");
        String mensaje = sc.nextLine();

        System.out.println("tu nombre ");
        String nombre = sc.nextLine();

        Mensajes registro = new Mensajes();

        registro.setMensaje(mensaje);
        registro.setAutor_mensaje(nombre);
        MensajesDAO.CrearMensajeDB(registro);

    }

    public static void listarMensajes() {

        MensajesDAO.leerMensajesDB();

    }

    public static void borrarMensaje() {

        Scanner sc = new Scanner(System.in);
        System.out.println("Indica el Id del mensaje");
        int id_mensaje = sc.nextInt();
        MensajesDAO.borrarMensajeDB(id_mensaje);

    }

    public static void editarMensaje() {

        Scanner sc = new Scanner(System.in);

        System.out.println("escribe el mensaje ");
        String mensaje = sc.nextLine();

        System.out.println("Escribe el id del mensaje que quieres actualizar");
        int id_mensaje = sc.nextInt();

        Mensajes actualizacion = new Mensajes();

        actualizacion.setId_mensaje(id_mensaje);
        actualizacion.setMensaje(mensaje);
        MensajesDAO.actualizarMensajeDB(actualizacion);

    }
}
