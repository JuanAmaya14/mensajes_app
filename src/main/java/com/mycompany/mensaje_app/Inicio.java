package com.mycompany.mensaje_app;

import java.sql.Connection;
import java.util.Scanner;

public class Inicio {
    
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);
        
        int opcion = 0;
        
        do{
            System.out.println("---------------------"
                    + "\n Aplicacion de mensajes"
                    + "\n 1. crear mensajes"
                    + "\n 2. listar mensajes"
                    + "\n 3. eliminar mensajes"
                    + "\n 4. editar mensajes"
                    + "\n 5. salir");
            
                    opcion = sc.nextInt();
                    
                    switch(opcion){
                        case 1:
                            mensajesService.crearMensaje();
                            break;
                        case 2:
                            mensajesService.listarMensajes();
                            break;
                        case 3:
                            mensajesService.borrarMensaje();
                            break;
                        case 4:
                            mensajesService.editarMensaje();
                            break;
                        default:
                            break;
                    }
            
            
            
            
        }while(opcion != 5);
        
        
        
        
    }
    
}
